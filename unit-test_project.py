import unittest

def myBigNumber(stn1, stn2):
  #Luôn đảm bảo số a có độ dài lớn hơn số b
  if len(stn1) < len(stn2):
    stn1, stn2 = stn2, stn1
  #Tạo list cho số a và số b
  stn1 = list(stn1)
  stn2 = list(stn2)
  total = int(sumNumbers(stn1, stn2))
  return total

def sumNumbers(stn1, stn2):
  temp = 0
  result = ''
  lenMax = max(len(stn1),len(stn2))

  for i in range(lenMax):
  # Chuyển đổi số từ ký tự sang số nguyên
    idx1 = len(stn1) - i - 1 
    idx2 = len(stn2) - i - 1
    digit1 = stn1[idx1] if idx1 >= 0 else '0'
    digit2 = stn2[idx2] if idx2 >= 0 else '0'
    
  #Tính tổng của chúng từ phải qua trái
    temp_sum = int(digit1) + int(digit2) + temp
    print(str(digit1) + "+" + str(digit2) + "+" + str(temp) 
                      + "=" + str(temp_sum))
    if temp_sum >= 10:
      temp = 1
      temp_sum -= 10
      print("Write:",temp_sum, "- Remember: 1")
    else:
      print("Write:",temp_sum)
      temp = 0
    result = str(temp_sum) + result
  return result

#Trường hợp stn1 hoặc stn2 không là con số
def isNotNumber(stn1,stn2):
  if stn1.isnumeric() == False:
    if stn2.isnumeric() == False:
      raise Exception("The first number '{}' is not numeric".format(stn1),
                      "The second number '{}' is not numeric".format(stn2))
    else:
      raise Exception("The first number '{}' is not numeric".format(stn1))
  elif stn2.isnumeric() == False:
    raise Exception("The second number '{}' is not numeric".format(stn2))
  else:
    return True

#Trường hợp stn1 hoặc stn2 là số âm:
def numberFormatException(stn1,stn2):
  stn1, stn2 = int(stn1), int(stn2)

  if stn1 < 0:
     raise Exception("System is not support for negative number")
  elif stn2 < 0:
    raise Exception("System is not support for negative number")
  else:
    return True

class SumTest(unittest.TestCase):
  def test_sumfunc_1(self):
    #Arrange:
    a = '1234'
    b = '897'
    #Act
    result = myBigNumber(a,b)
    #Assert
    self.assertEqual(result, int(a)+int(b))
  
  def test_sumfunc_2(self):
    #Arrange:
    a = '1234'
    b = '897'
    #Act
    result = myBigNumber(b,a)
    #Assert
    self.assertEqual(result, int(a)+int(b))

  # def test_sumfunc_3(self):
  #   #Arrange:
  #   a = '1234'
  #   b = '897c'
  #   self.assertEqual(isNotNumber(a,b),True)

  # def test_sumfunc_4(self):
  #   #Arrange:
  #   a = '-1234'
  #   b = '897'
  #   self.assertEqual(numberFormatException(a,b),True)

if __name__ == "__main__":
  unittest.main()