# Đối với file Jupyter Notebook: (Dùng để Code)
- Input vào 2 số a và b bất kì (theo đề bài: 1234 và 897 dưới dạng chuỗi).
- Viết hàm sumNumbers với các tham số truyền vào lần lượt là 2 số a và b.
- Viết hàm myBigNumber để xuất ra giá trị nguyên của tổng 2 số vừa cộng.

#### Đối với hàm myBigNumber:
- Đưa 2 số a và b về đúng vị trí của nó trước khi thực hiện phép tính cộng.
- Tạo mảng để chứa cái ký tự của 2 số a và b.
- Sau đó, thực hiện hàm sumNumbers.

$\Rightarrow$ Sau đó ta có được tổng của 2 số a và b theo kiểu dữ liệu 'int'.

#### Đối với hàm sumNumbers:
- Khởi tạo biến temp, temp_sum và result
    - temp để lưu các giá trị sau khi thực hiện phép tính cộng(nếu phép tính cộng giữa 2 số >=10 thì temp = 1).
    - temp_sum để lưu các giá trị sau khi thực hiện phép tính cộng(nếu phép tính cộng giữa 2 số >=10 thì temp_sum lưu giá trị của hàng đơn vị của phép tính).
    - result là chuỗi kí tự để lưu kết quả cuối cùng "viết ra giấy". 
- Đưa các giá trị của mảng stn1 và stn2 về giá trị nguyên.
- Sau đó, cộng lần lượt cái phần tử từ phải sang của mỗi mảng, nếu tổng của chúng >= 10 thì lưu giá trị này vào biến "temp" = 1 để phép tính tiếp theo khi cộng ta thêm 1 vào.
- Tính tuần tự cho đến khi hết 2 mảng.
- Trường hợp stn1 > stn2 thì khi đã đi hết chuỗi của stn2 thì giá trị của stn2 tại vị trí -1 sẽ nhận giá trị '0'.

$\Rightarrow$ Khi đó, ta có được giá trị của result là 1 mảng kết quả tổng của 2 số vừa cộng, mảng này có kiểu dữ liệu là kiểu 'str'.

# Đối với file Python: (Dùng để TestCase)
- Đưa các hàm vừa viết vào file.
- Sau đó cho các trường hợp test.
- Sau đó thực hiện test với unittest.main()

#### Với hàm test_sumfunc_1:
- Arrange: cho 2 số bất kỳ a và b (Theo đề bài 1234 và 897 dưới dạng chuỗi)
- Action: Tính kết quả của 2 số vừa cho theo hàm đã code.
- Assert: Dùng hàm self.assertEqual của thư viện unittest với các tham số truyền vào là phần 'Action' và  giá trị thực khi tính của nó.

#### Với hàm test_sumfunc_2:
- Arrange: cho 2 số bất kỳ a và b (Theo đề bài 1234 và 897 dưới dạng chuỗi)
- Action: Tính kết quả của 2 số vừa cho theo hàm đã code (thay đổi vị trí của a và b so với test_sumfunc_1).
- Assert: Dùng hàm self.assertEqual của thư viện unittest với các tham số truyền vào là phần 'Action' và  giá trị thực khi tính của nó.

### Với hàm test_sumfunc_3:
- Dùng để kiểm tra chuỗi của 2 stn1 và stn2 có phải là con số hay không và báo lỗi.

### Với hàm test_sumfunc_4:
- Dùng để kiểm tra chuỗi của 2 stn1 và stn2 là số âm hay không và báo lỗi.
